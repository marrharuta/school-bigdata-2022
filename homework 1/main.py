import pandas as pd
import argparse
import pyarrow


def csv_to_parquet(path_infile, path_outfile):
    """
    Function for convert csv to parquet
    """
    df = pd.read_csv(path_infile)
    df.to_parquet(path_outfile)


def parquet_to_csv(path_infile, path_outfile):
    """
    Function for convert parquet to csv
    """
    df = pd.read_parquet(path_infile)
    df.to_csv(path_outfile,  index=False)


def get_scheme(parquet_path):
    """
    Function for fet scheme of parquet file
    """
    df = pd.read_parquet(parquet_path)
    schema = pyarrow.Table.from_pandas(df=df).schema
    return schema


def get_argument_parser():
    """
    Get argument from terminal
    """
    argument = argparse.ArgumentParser()

    argument.add_argument("--csv2parquet", type=str,
                          help="Convert csv to parquet. Set input csv filename string")
    argument.add_argument("--parquet2csv", type=str,
                          help="Convert parquet to csv. Set input parquet filename string")
    argument.add_argument("--get_schema", type=str,
                          help="Get schema of parquet file. Set input parquet filename string")

    return vars(argument.parse_args())


def main():
    arg = get_argument_parser()

    if arg['csv2parquet']:
        input_file = arg['csv2parquet']
        csv_to_parquet(input_file, "outputfile.parquet")

    elif arg['parquet2csv']:
        input_file = arg['parquet2csv']
        parquet_to_csv(input_file, "outputfile.csv")

    elif arg['get_schema']:
        print(get_scheme(arg['get_schema']))


if __name__ == '__main__':
    main()

