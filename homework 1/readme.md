<h1>DATA FILES CONVERTER</h1>
<h3>A console program allows user to convert different data types</h3>
<hr>

<h2>PARAMETERS</h2>
*--csv2parquet* — convert csv to parquet<br>
*--parquet2csv* — convert parquet to csv<br>
*--get_schema*  — get parquet schema<br>
*--help* — get help about use program
<hr>

<h2>USAGE</h2>
1. You needs libraries from para requirements (to correctly job)
2. Enter parameters in terminal to perform one of the function
```python
>python main.py --parameter name_file.format
```
3. You'll can see result in terminal if you use *--get_schema* or *--help*. 
If you use *--csv2parquet* or *--parquet2csv* you'll can see new file. 


