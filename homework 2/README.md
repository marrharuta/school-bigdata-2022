<h1 align="center">GET MOVIES</h1>

<h3>A console program allows user to get top N movies from different genres and years in csv-like format.There are 5 optional parameters for user help in films choice.</h3>

<hr>

<h2>PARAMETERS</h2>
<i> -h, -help </i> — show help message<br>
<i> -N </i> — choice number of films<br>
<i> -genre </i> — choice film genre/genres<br>
<i> -year_from </i> — choice start of film year<br>
<i> -year_to </i> — choice finish of film year<br>
<i> -regexp </i> — choice movie by name or part of name
<hr>

<h2>USAGE</h2>
You will be able to run the program if the python file and the data files are in the same folder<br>
You can specify one or some of the optional parameters:<br>
```python
> python get-movies.py [-h] [-N N] [-genre GENRE] [-year_from YEAR_FROM] [-year_to YEAR_TO] [-regexp REGEXP]
```
You can write one or some film genres:
```python
-genre Action
-genre "Comedy|Children"
```

Example of use:
```python
> python get-movies.py  -N 3 -genre "Fantasy|Western" -year_from 1990 -year_to 2010
```
```
genres;title;year;rating
Fantasy;Colourful (Karafuru);2010;5.0
Fantasy;Superman/Batman: Public Enemies;2009;5.0
Fantasy;Wonder Woman;2009;5.0
Western;Sweetgrass;2009;4.5
Western;Texas - Doc Snyder hält die Welt in Atem;1993;4.5
Western;There Will Be Blood;2007;4.1429
```
