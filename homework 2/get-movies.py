import argparse
import re


def argument_parser():
    """Function for get needed arguments from console"""

    arg = argparse.ArgumentParser()

    arg.add_argument("-N", type=int,
                     help="Allow you to get top N movies")
    arg.add_argument("-genre", type=str,
                     help="Allow you to choice movie genre")
    arg.add_argument("-year_from", type=int,
                     help="Allow you to choice start of time period")
    arg.add_argument("-year_to", type=int,
                     help="Allow you to choice finish of time period")
    arg.add_argument("-regexp", type=str,
                     help="Allow you to find movie by name or part of name")
    return arg.parse_args()


def get_rating():
    """Get two dictionary from rating.csv and return its"""

    ratings = open("ratings.csv", "r")
    sum_rating = dict()
    count_rating = dict()

    for row in ratings:
        row = row.split(',')
        if row[1].isdigit():
            if row[1] in sum_rating:
                sum_rating[row[1]] += float(row[2])
                count_rating[row[1]] += 1
            else:
                sum_rating[row[1]] = float(row[2])
                count_rating[row[1]] = 1
        else:
            continue

    return sum_rating, count_rating


def split_genre(movies_list):
    """Create list with split genre"""

    genre_list = []
    split_list = []
    for row in movies_list:
        genres = row[0].split("|")
        for genre in genres:
            if genre == "(no genres listed)":
                continue
            if genre in genre_list:
                k = 0
            else:
                genre_list.append(genre)
            movie_list = []
            movie_list.append(genre)
            movie_list.extend(row[1:])
            split_list.append(movie_list)
    genre_list.sort()

    return split_list, genre_list


def add_rating_to_list(movie_list, bad_data):
    """Get two dictionary from average rating
       and add it to movies list as format:
       genre; title; year; rating
       movies list sorted by year"""

    sum_rating, count_rating = get_rating()
    movies_list = []
    for row in movie_list:
        if row[0] in sum_rating.keys():
            average_rating = round(sum_rating[row[0]] / count_rating[row[0]], 4)
            new_line = ";".join(row)
            lft_ind = new_line.find(';')
            rgt_ind = new_line.rfind(';')
            new_line = new_line[rgt_ind+1:] + new_line[lft_ind:rgt_ind] + ';' + str(average_rating)
            new_line = new_line.split(';')
            movies_list.append(new_line)
        else:
            new_line = ",".join(row)
            bad_data.append(new_line)

    movies_list = sorted(movies_list, key=lambda row: row[3], reverse=True)
    movies_list, genre_list = split_genre(movies_list)

    return movies_list, bad_data, genre_list


def sort_by_year(movie_list):
    """Sort movies list by year
       get sorted list by two rows: year & rating"""

    sort_list = []
    sorted_list = []
    check = movie_list[0][3]
    for row in movie_list:
        if row[3] != check:
            sort_list = sorted(sort_list, key=lambda row: row[2], reverse=True)
            sorted_list.extend(sort_list)
            sort_list.clear()
            check = row[3]
            sort_list.append(row)
        else:
            sort_list.append(row)

    return sorted_list


def sort_by_title(movie_list):
    """Sort movies list by title
       get sorted list by three rows: title & year & rating"""

    sort_list = []
    sorted_list = []
    year = movie_list[0][2]
    rating = movie_list[0][3]
    for row in movie_list:
        if row[3] != rating:
            rating = row[3]
            year = row[2]
            sort_list.clear()
            sort_list.append(row)
        else:
            if row[2] != year:
                sort_list = sorted(sort_list, key=lambda row: row[1])
                sorted_list.extend(sort_list)
                sort_list.clear()
                year = row[2]
                sort_list.append(row)
            else:
                sort_list.append(row)

    return sorted_list


def check_list_to_sort(arg_dict, movie_list, genre_list):
    """Sort movie list if not genre in argument"""

    movies_list = []
    for genre in genre_list:
        count = 0
        for i in range(len(movie_list)):
            if genre in movie_list[i]:
                if count == arg_dict['top_n']:
                    break
                else:
                    movies_list.append(movie_list[i])
                    count += 1
            else:
                continue
    return movies_list


def check_genre_and_top_n(arg_dict, movie_list):
    """Check genre or genres of film
       check how much film we should print"""

    movies_list = []
    if '|' in arg_dict['genre']:
        genres = arg_dict['genre'].split('|')
        for row in genres:
            count = 0
            for i in range(len(movie_list)):
                if row in movie_list[i]:
                    if count == arg_dict['top_n']:
                        break
                    else:
                        movies_list.append(movie_list[i])
                        count += 1
                else:
                    continue
    else:
        count = 0
        for i in range(len(movie_list)):
            if arg_dict['genre'] in movie_list[i]:
                if count == arg_dict['top_n']:
                    break
                else:
                    movies_list.append(movie_list[i])
                    count += 1
            else:
                continue

    return movies_list


def check_year(arg_dict, movie_list):
    """Check time period of film"""

    movies_list = []
    for i in range(len(movie_list)):
        if arg_dict['year_from'] and not arg_dict['year_to']:
            if arg_dict['year_from'] <= int(movie_list[i][2]):
                movies_list.append(movie_list[i])
        elif arg_dict['year_to'] and not arg_dict['year_from']:
            if arg_dict['year_to'] >= int(movie_list[i][2]):
                movies_list.append(movie_list[i])
        else:
            if arg_dict['year_from'] <= int(movie_list[i][2]) <= arg_dict['year_to']:
                movies_list.append(movie_list[i])
    return movies_list


def check_regexp(arg_dict, movie_list):
    """Check part film name or film name in title"""

    movies_list = []
    for i in range(len(movie_list)):
        if arg_dict['regexp'].lower() in movie_list[i][1].lower():
            movies_list.append(movie_list[i])
    return movies_list


def write_bad_data(bad_data):
    bad_data_file = open("bad_data.csv", "w")
    for line in bad_data:
        line = "".join(line)
        bad_data_file.write(line)

def extract_movies(arg_dict):
    """Parse year and create bad data list
       Add average rating to list
       Sort list by two rows: year & rating
       Check arguments and compare to our movies list
       Print results"""

    movies = open("movies.csv", "r", encoding='utf-8')
    movies_list = []
    bad_data = []
    line = movies.readline()
    while line:
        line = movies.readline()
        if not line:
            break

        year = re.search(r'[0-9]{4}\)', line)
        if year == None:
            bad_data.append(line)
            continue
        else:
            year = year.group(0)
        line = re.sub(r'\s\([0-9]{4}\)', "", line)
        index = line.rfind(',')
        new_year = re.sub('\)', "", year)
        new_line = line[:index] + ';' + new_year + ';' + line[index+1:]
        new_line = new_line.replace(',', ';', 1)

        new_line = new_line[:-1].split(';')
        movies_list.append(new_line)

    movies_list, bad_data, genre_list = add_rating_to_list(movies_list, bad_data)
    movies_list = sort_by_year(movies_list)
    movies_list = sort_by_title(movies_list)

    write_bad_data(bad_data)

    if arg_dict['year_from'] or arg_dict['year_to']:
        movies_list = check_year(arg_dict, movies_list)
    if arg_dict['regexp']:
        movies_list = check_regexp(arg_dict, movies_list)
    if arg_dict['genre']:
        movies_list = check_genre_and_top_n(arg_dict, movies_list)
    if not arg_dict['genre']:
        movies_list = check_list_to_sort(arg_dict, movies_list, genre_list)

    print('genres;title;year;rating')
    for i in range(len(movies_list)):
        print(";".join(map(str, movies_list[i])))


def main():
    arg = argument_parser()
    arg_dict = {
        'genre': arg.genre,
        'top_n': arg.N,
        'year_from': arg.year_from,
        'year_to': arg.year_to,
        'regexp': arg.regexp
    }
    extract_movies(arg_dict)


if __name__ == '__main__':
    main()
