<h1>GET MOVIES</h1>

<h3>A client-server program allows user to get top N movies from different genres and years in csv-like format.There are 5 optional parameters for user help in films choice.</h4>
<hr>

<h2>PARAMETERS</h2>
<i> -h, -help </i> — show help message<br>
<i> -N </i> — choice number of films<br>
<i> -genre </i> — choice film genre/genres<br>
<i> -year_from </i> — choice start of film year<br>
<i> -year_to </i> — choice finish of film year<br>
<i> -regexp </i> — choice movie by name or part of name
<hr>

<h2>USAGE</h2>
```python
> python main.py [-h] [-N N] [-genre GENRE] [-year_from YEAR_FROM] [-year_to YEAR_TO] [-regexp REGEXP]
```
You can write one or some film genres:
```python
-genre Action
-genre "Comedy|Children"
```

Example of use:
```python
> python get-movies.py  -N 3 -genre "Fantasy|Western" -year_from 1990 -year_to 2010
```
```
title; year; genres; rating
Colourful (Karafuru);2010;Fantasy;5.0
Superman/Batman: Public Enemies;2009;Fantasy;5.0
Wonder Woman;2009;Fantasy;5.0
Sweetgrass;2009;Western;4.5
Texas - Doc Snyder hält die Welt in Atem;1993;Western;4.5
Lone Star;1996;Western;4.1579
```
<hr>
<h2>INSTALL</h2>
For work you need database <i>movies_db</i>. Run python file <i>setup_db</i> for install database.
Expample of install:
```python
> python setup_db.py
```
