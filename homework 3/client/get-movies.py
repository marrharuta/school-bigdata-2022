import argparse
from mysql.connector import connect
import configparser


def get_config():
    """
    Read config file and get it's parameters
    """
    def read_section(config_server, section):
        config = configparser.ConfigParser()
        parameter_list = []

        config.read(config_server)

        for parameter_name in config[section]:
            parameter_list.append(config[section][parameter_name])

        return parameter_list

    config_server = "config.ini"
    config_db, config_parse = [list() for _ in range(2)]

    config_db.extend(read_section(config_server, 'DB'))
    config_parse.extend(read_section(config_server, 'PARSE'))

    return config_db, config_parse


def argument_parser():
    """
    Get needed arguments from console
    """
    arg = argparse.ArgumentParser()

    arg.add_argument("-N", type=int,
                     help="Allow you to get top N movies")
    arg.add_argument("-genre", type=str,
                     help="Allow you to choice movie genre")
    arg.add_argument("-year_from", type=int,
                     help="Allow you to choice start of time period")
    arg.add_argument("-year_to", type=int,
                     help="Allow you to choice finish of time period")
    arg.add_argument("-regexp", type=str,
                     help="Allow you to find movie by name or part of name")
    return vars(arg.parse_args())


def execute_query(conn, top_n=None, regexp=None, year_from=None, year_to=None, genres=None):
    """
    Function to send query to server
    """
    if not top_n:
        top_n = 'NULL'
    if not regexp:
        regexp = 'NULL'
    else:
        regexp = f"'{regexp}'"
    if not year_from:
        year_from = 'NULL'
    if not year_to:
        year_to = 'NULL'
    if not genres:
        genres = 'NULL'
    else:
        genres = f"'{genres}'"

    cursor = conn.cursor()

    try:
        if top_n == 'NULL':
            query = f"CALL movies_db.perform_query_all({regexp}, {year_from}, {year_to}, {genres});"
        else:
            query = f"CALL movies_db.perform_query_N({top_n}, {regexp}, {year_from}, {year_to}, {genres});"
        for c in cursor.execute(query, multi=True):
            if c.with_rows:
                for row in c.fetchall():
                    yield row
    except Exception as e:
        print(e)

    cursor.close()


def print_movies(conn, header, delimiter, movies_genres, top_n=None, regexp=None, year_from=None, year_to=None, genres=None):
    """
    Print data from server in csv-like format
    """
    try:
        print(header)

        if not genres:
            genres = movies_genres

        genres = genres.split('|')
        genres = [f"{g}" for g in genres]

        for genre in genres:
            for row in execute_query(conn, top_n, regexp, year_from, year_to, genre):
                data_row = ''
                for item in row:
                    if delimiter in str(item):
                        item = f'"{item}"'
                    data_row += delimiter + str(item)
                data_row = data_row[1:]
                print(data_row)

    except Exception as e:
        print(e)


def main():
    """
    Connection to database and print query result
    """
    config_db, config_parse = get_config()
    host_name, port, user_name, user_password, database = config_db
    header, delimiter, movies_genres = config_parse

    args = argument_parser()
    top_n = args['N']
    regexp = args['regexp']
    year_from = args['year_from']
    year_to = args['year_to']
    genres = args['genre']

    try:
        with connect(
                host=host_name,
                port=port,
                user=user_name,
                passwd=user_password,
                database=database
        ) as connection:
            print_movies(connection, header, delimiter, movies_genres, top_n, regexp, year_from, year_to, genres)
        connection.close()
    except Exception as e:
        print(f"The error '{e}' occurred")


if __name__ == "__main__":
    main()