USE movies_db;
DROP PROCEDURE IF EXISTS `perform_query_all`;
CREATE PROCEDURE `perform_query_all`(
				IN `regexp` VARCHAR(255),
				IN year_from INT,
				IN year_to INT,
				IN genres VARCHAR(255)
)
SELECT m.genre, m.title, m.movie_year, m.rating
  FROM movies AS m
	WHERE (genres IS NULL) OR (REGEXP_SUBSTR(genres, m.genre) != '') 
    AND ((year_from IS NULL) OR m.movie_year >= year_from)  
    AND ((year_to IS NULL) OR (m.movie_year <= year_to))
	  AND ((`regexp` IS NULL) OR (REGEXP_SUBSTR(m.title, `regexp`) != ''))
ORDER BY m.genre, m.rating DESC, m.movie_year DESC, m.title ASC