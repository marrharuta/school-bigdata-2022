USE movies_db;
DROP TABLE IF EXISTS movies;
CREATE TABLE movies
(
	movie_id		    INT				NOT NULL,
	genre			    CHAR(255)		NOT NULL,
    title		        CHAR(255)		NOT NULL,
    movie_year		    INT				NOT NULL,
    rating				FLOAT			NOT NULL
);