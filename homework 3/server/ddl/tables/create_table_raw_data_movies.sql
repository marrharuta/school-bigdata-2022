USE movies_db;
DROP TABLE IF EXISTS raw_data_movies;
CREATE TABLE raw_data_movies
(
	movie_id		INT 		NOT NULL,
    title 		CHAR(255)	NOT NULL,
    movie_year  INT			NOT NULL,
    genres		CHAR(255)	NOT NULL
);