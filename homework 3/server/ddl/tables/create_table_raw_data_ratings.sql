USE movies_db;
DROP TABLE IF EXISTS raw_data_ratings;
CREATE TABLE raw_data_ratings
(
	user_id			INT 		NOT NULL,
	movie_id		INT			NOT NULL,
    rating			FLOAT		NOT NULL,
    timestamp_		CHAR(255)	NOT NULL
);