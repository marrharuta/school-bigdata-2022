USE movies_db;
INSERT INTO movies (movie_id, genre, title, movie_year, rating) 
WITH cte AS(
	WITH s_cte AS(
		SELECT rdm.movie_id, 
			   rdm.title, 
			   rdm.movie_year, 
			   rdm.genres, 
			   ROUND(AVG(rating),4) AS rating
		FROM   raw_data_movies  AS rdm
		JOIN   raw_data_ratings AS rdr
		  ON rdm.movie_id = rdr.movie_id
		GROUP BY rdm.movie_id
    )
	SELECT movie_id, title, movie_year, jt.genre, rating, sc.genres
    FROM s_cte AS sc
    JOIN JSON_TABLE(
		 REPLACE(json_array(sc.genres), '|', '","'),'$[*]' COLUMNS (genre VARCHAR(255) PATH '$')) as jt
)
SELECT movie_id, k.genre, title, movie_year, rating
FROM   cte AS k;
DELETE FROM movies WHERE genre = "(no genres listed)" OR movie_year = 0