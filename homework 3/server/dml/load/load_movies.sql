USE movies_db;
LOAD DATA INFILE 		'C:\movies_small.csv'
INTO TABLE 				raw_data_movies
FIELDS TERMINATED BY 	','
ENCLOSED BY 			'"'
LINES TERMINATED BY 	'\r\n'
IGNORE 1 LINES
(movie_id, title, genres)
SET movie_year = IF(title REGEXP('\\([0-9]{4}\\)'), cast(regexp_substr(title,'[0-9]{4}') AS UNSIGNED), 0),
title = regexp_replace(title, '\\([0-9]{4}\\)', ' ')