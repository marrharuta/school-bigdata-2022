USE movies_db;
LOAD DATA INFILE 		'C:/ratings_small.csv'
INTO TABLE 				raw_data_ratings
FIELDS TERMINATED BY 	','
LINES TERMINATED BY 	'\n'
IGNORE 1 LINES;