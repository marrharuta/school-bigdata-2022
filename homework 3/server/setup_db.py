from mysql.connector import connect
import configparser
from os import walk
import sys


def get_config():
    """
    Read config file and get it's parameters
    """
    def read_section(config_server, section):
        config = configparser.ConfigParser()
        parameter_list = []

        config.read(config_server)

        for parameter_name in config[section]:
            parameter_list.append(config[section][parameter_name])

        return parameter_list

    config_server = "config.ini"
    config_db, config_sql = [list() for _ in range(2)]

    config_db.extend(read_section(config_server, 'DB'))
    config_sql.extend(read_section(config_server, 'SQL'))

    return config_db, config_sql


def execute_query(sql_file, connection):
    """
    Execute received script
    """
    cursor = connection.cursor()
    with open(sql_file, 'r') as file:
        queries = file.read()
        for query in queries.split(';'):
            if len(query) == 0:
                continue
            cursor.execute(query)
        connection.commit()


def main():
    """
    Setup database in MySQL
    """
    config_db, config_sql = get_config()
    host_name, port, user_name, user_password = config_db

    try:
        with connect(
                host=host_name,
                port=port,
                user=user_name,
                passwd=user_password
        ) as connection:
            for path in config_sql:
                for (folder_path, _, filenames) in walk(path):
                    for sql_filename in filenames:
                        print('Script "{}" in progress...'.format(sql_filename), file=sys.stderr)
                        execute_query(folder_path + sql_filename, connection)
        print("Setup database to MySQL successful")
        connection.close()
    except Exception as e:
        print(f"The error '{e}' occurred")


if __name__ == '__main__':
    main()

